//
//  OrderCell.swift
//  Wash
//
//  Created by ramy nasser on 9/30/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var ThirdStepView: UIView!
    @IBOutlet weak var ThirdStepImage: UIImageView!
    @IBOutlet weak var FourSecondView: UIView!
    @IBOutlet weak var FourStepImage: UIImageView!
    @IBOutlet weak var FourStepView: UIView!
    
    @IBOutlet weak var SecondStepImage: UIImageView!
    @IBOutlet weak var SecondStepView: UIView!
    @IBOutlet weak var FirstStepView: UIView!
    @IBOutlet weak var FirstStepImage: UIImageView!
    @IBOutlet weak var FourStepLabel: UILabel!
    @IBOutlet weak var ThirdStepLabell: UILabel!
    @IBOutlet weak var SecondStepLabel: UILabel!
    @IBOutlet weak var FirstStepLabel: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var orderDateType: UILabel!
    @IBOutlet weak var PaymentTypeLabel: UILabel!
    @IBOutlet weak var ServiceNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
