//
//  DefaultsKeys.swift
//  Kia
//
//  Created by Magdy Zamel on 7/13/17.
//  Copyright © 2017 Magdy Zamel. All rights reserved.
//

import Foundation

enum DefaultsKeys {
    static let useToken = "Token"
    static let fcmkey = "fcmkey"
//    static let carId = "carId"
//    static let settingNotif = "Notif"
//    static let settingLang = "Lang"
//    static let PasswordKey = "Password"

}
