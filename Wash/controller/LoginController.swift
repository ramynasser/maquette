//
//  LoginController.swift
//  Wash
//
//  Created by ramy nasser on 9/27/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBAction func GoTo(_ sender: AnyObject) {
        let view = self.storyboard!.instantiateViewController(withIdentifier: "SignUpController")as! SignUpController
        self.navigationController?.pushViewController(view, animated: true)

        
    }
    @IBAction func login(_ sender: AnyObject) {
    
        if validtor() {
            UiHelpers.showLoader()
            
            if let username = self.usernameField.text , let password = self.passwordField.text {
                let parmeters = [
                    "username": "\(username)",
                    "password": "\(password)"
                ]
                Alamofire.request(BASEDOMAIN.appending("/login"), method: .post, parameters: parmeters, encoding: JSONEncoding.default)
                    .responseJSON { (response) in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            if  response.response?.statusCode == 400 {
                                UiHelpers.hideLoader()
                            }
                            let post = JSON(value)
                            print(JSON(value))
                            if let token = post["accessToken"].string {
                                print("the token y ramy \(token)")
                                UiHelpers.hideLoader()
                                userDefaults.set(token, forKey: DefaultsKeys.useToken)
                                let view = self.storyboard!.instantiateViewController(withIdentifier: "CustomSideMenuController")as! CustomSideMenuController
                                self.present(view, animated: true, completion: nil)
                                
                            }
                                
                            else {
                                UiHelpers.hideLoader()
                                DataUtlis.data.ErrorDialog(Title: "خطا", Body: "كلمة السر او اسم المستخدم خاطئة")
                                print("error \(response.error)")
                            }
                            
                            
                        }
                }
            }
        }
            
                        

                        
         
                        else {
            DataUtlis.data.ErrorDialog(Title: "ERROR", Body: "NOT REGISTERED USER")

        }
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.usernameField.delegate = self
        self.passwordField.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.usernameField.resignFirstResponder()
        
        self.passwordField.resignFirstResponder()
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        // textField.resignFirstResponder()
        self.usernameField.resignFirstResponder()
        
        self.passwordField.resignFirstResponder()
        
        
        return true
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
                
                
                
            }}
        
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status and drop into background
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func validtor() -> Bool {
        var valid = false
        
        if (usernameField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال اسم المستخدم")
            valid = false
            
        }
            
        else if (passwordField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال كلمة المرور")
            valid = false
            
        }
            
            
        else {
            valid = true
        }
        return valid
    }
    

    
}
