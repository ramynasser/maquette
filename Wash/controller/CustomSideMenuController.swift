//
//  CustomSideMenuController.swift
//  Example
//
//  Created by Teodor Patras on 16/06/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import Foundation
import SideMenuController
import Alamofire
class CustomSideMenuController: SideMenuController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performSegue(withIdentifier: "showCenterController1", sender: nil)
        performSegue(withIdentifier: "containSideMenu", sender: nil)
        let header:HTTPHeaders = ["x-access-Token":userDefaults.string(forKey: DefaultsKeys.useToken)!]
        if let token = userDefaults.string(forKey: DefaultsKeys.fcmkey){

        let parmeters = [
            "deviceType": "ios",
            "deviceToken": "\(token)"
        ]
        Alamofire.request(BASEDOMAIN.appending("/register-device"), method: .post, parameters: parmeters, encoding: JSONEncoding.default, headers:header)
            .responseJSON { (response) in
                if let value: AnyObject = response.result.value as AnyObject? {
                    if  response.response?.statusCode == 400 {
                        UiHelpers.hideLoader()
                    }
                    if  response.response?.statusCode == 201 {
                        UiHelpers.hideLoader()
                    }
                    
                }
        
        }
    }
    }
    }




 
