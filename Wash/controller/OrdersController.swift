//
//  OrdersController.swift
//  Wash
//
//  Created by ramy nasser on 9/30/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit

class OrdersController: UITableViewController {
    lazy var cleanerRepository :CleanerRepository = {
        let Repo  = CleanerRepository()
        return Repo
    }()
    var response = [OrderRepsonseApi]()
    override func viewDidLoad() {
        super.viewDidLoad()
        FetchOrders()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return response.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as!
        OrderCell
        let order = self.response[indexPath.row]
        
        cell.ServiceNameLabel.text = "Service Type : \(order.typeOfService.typeOfService!)"
        cell.PaymentTypeLabel.text = "Payment Type : \(order.payment!)"
        cell.PriceLabel.text = "Price : \(order.price!)"
        if let lowerBound = order.createdAt!.range(of: "T")?.lowerBound {
            cell.orderDateType.text = "order date : \(order.createdAt!.substring(to: lowerBound))"

        }

        
        if order.status[0].state == true {
            cell.FirstStepView.backgroundColor = UIColor(rgb: 0xF05D29)
            cell.FirstStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette (4)")
            cell.FirstStepLabel.text = order.status[0].name
        }else {
            cell.FirstStepView.backgroundColor = UIColor.white
            cell.FirstStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette")
            cell.FirstStepLabel.text = order.status[0].name

        }
        
        if order.status[1].state  == true {
            cell.SecondStepView.backgroundColor = UIColor(rgb: 0xF05D29)
            cell.SecondStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette (4)")
            cell.SecondStepLabel.text = order.status[1].name


        }else {
            cell.SecondStepLabel.text = order.status[1].name
            cell.SecondStepView.backgroundColor = UIColor.white
            cell.SecondStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette")

        }
        if order.status[2].state  == true  {
            cell.ThirdStepView.backgroundColor = UIColor(rgb: 0xF05D29)
            cell.ThirdStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette (4)")
            cell.ThirdStepLabell.text = order.status[2].name


        }else {
            cell.ThirdStepView.backgroundColor = UIColor.white
            cell.ThirdStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette")
            cell.ThirdStepLabell.text = order.status[2].name

        }
        if order.status[3].state == true  {
            cell.FourStepView.backgroundColor = UIColor(rgb: 0xF05D29)
            cell.FourSecondView.backgroundColor = UIColor(rgb: 0xF05D29)
            cell.FourStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette (4)")

            cell.FourStepLabel.text = order.status[3].name

        }else {
            cell.FourStepView.backgroundColor = UIColor.white
            cell.FourStepImage.image = #imageLiteral(resourceName: "circular-shape-silhouette")
            cell.FourSecondView.backgroundColor = UIColor.white
            cell.FourStepLabel.text = order.status[3].name


        }
        // Configure the cell...

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    

}
extension OrdersController {
    func FetchOrders()  {
        UiHelpers.showLoader()
        cleanerRepository.GetOrders{ (response, statusCode) in
            switch statusCode {
            case 200 :
                UiHelpers.hideLoader()
                self.response = response!
                self.tableView.reloadData()
                
                
                
                
            default :
                UiHelpers.hideLoader()
                
                DataUtlis.data.ErrorDialog(Title: "خطا في الانترنت", Body: "تاكد من اتصالك بالانترنت")
                
                
            }
            
        }
    }
    
}


