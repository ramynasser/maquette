//
//  ErrorPopupVc.swift
//  Kia
//
//  Created by Magdy Zamel on 7/16/17.
//  Copyright © 2017 Magdy Zamel. All rights reserved.
//

import UIKit
import TextFieldEffects
protocol ErrorPopupVcDelegate: class {
    func errorPopupVc(width:Int , Height:Int)
}


class ErrorPopupVc: UIViewController {
    @IBOutlet weak var HeightTextField: HoshiTextField!
    @IBOutlet weak var WidthTextField: HoshiTextField!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var okButton: UIButton!

    var errorDescription:String!

    weak var delegate:ErrorPopupVcDelegate?

    
    override func viewDidLoad() {
        showAnimate()
        mainView.addEdagesShadow()
        
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(ErrorPopupVc.removeAnimate))
        self.view.addGestureRecognizer(dismissGesture)

    }
   


    @IBAction func okButtonTappad() {
        removeAnimate()
    }

     func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }


     func removeAnimate() {
        
      
        UIView.animate(withDuration: 0.3, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished) {
                
                if let delegate = self.delegate {
                    if let w = self.WidthTextField.text
                        , let h = self.HeightTextField.text {
                        if let width = Int(w) , let  height = Int(h) {
              
                            delegate.errorPopupVc(width:width , Height:height)
                        }else {
                        
                        }
                    }
                }
                self.view.removeFromSuperview()
            }
        })
    }
    
}


extension UIView  {

    func addEdagesShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.cornerRadius = 4
        self.layer.shouldRasterize = true
    }
}


