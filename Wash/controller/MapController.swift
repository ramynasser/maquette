//
//  MapController.swift
//  Wash
//
//  Created by ramy nasser on 9/29/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UIKit
import FirebaseCore
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Localize_Swift
import Segmentio
import FirebaseMessaging
import Gloss
import AVFoundation


class MapController: UIViewController {
    @IBAction func ShowNotificatiob(_ sender: AnyObject) {
        let view = self.storyboard!.instantiateViewController(withIdentifier: "NotificationController")as! NotificationController
        self.navigationController?.pushViewController(view, animated: true)

    }
    @IBOutlet weak var map: MKMapView!
    let locationManager = CLLocationManager()
    var location:UserLocation?
    var userLocation = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.addSideMenuButton()
        configerMap()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func WashMe(_ sender: AnyObject) {

        
        if let loc = self.location {
            let view = self.storyboard!.instantiateViewController(withIdentifier: "AddCarpetsVc")as! AddCarpetsVc
            view.location = self.location
            self.navigationController?.pushViewController(view, animated: true)

        }else {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "يجب ان تختار موقعك اولا")
        
        }
    
        
        
    }

    func configerMap() {
        map.delegate = self

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        map.showsUserLocation = true;
        
        let latDelta:CLLocationDegrees = 15
        let lonDelta:CLLocationDegrees = 15
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(userLocation.latitude, userLocation.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        map.setRegion(region, animated: false)
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = userLocation
        dropPin.title = "clinic name"
        dropPin.subtitle = "clincic address"
        map.addAnnotation(dropPin)

    }

    
    

}
extension MapController : CLLocationManagerDelegate,MKMapViewDelegate {
    
    func isAuthorizedtoGetUserLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            return false
        }
        return true
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "placeholder")
        }
        return annotationView
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }else{
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        var userLocation:CLLocation = locations[0] as CLLocation
        let span = MKCoordinateSpanMake(0.8, 0.8)
        var loction = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        print("Location long \(userLocation.coordinate.latitude)")
        print("Location lat \(userLocation.coordinate.longitude)")

        
        self.location = UserLocation(latitude:"\(userLocation.coordinate.latitude)" ,longitude:"\(userLocation.coordinate.longitude)" )
        self.userLocation = loction

        var region = MKCoordinateRegion(center: loction, span: span)
        self.map.setRegion(region, animated: true)
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}

struct UserLocation {

    var latitude:String?
    var longitude:String?
}
