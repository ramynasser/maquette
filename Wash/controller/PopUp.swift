//
//  PopUp.swift
//  Wash
//
//  Created by ramy nasser on 9/29/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit

class PopUp: UIViewController {

    
    @IBOutlet weak var HeightLayout: NSLayoutConstraint!
    @IBOutlet weak var BottomLayout: NSLayoutConstraint!
    @IBOutlet weak var PaymentSegment: UISegmentedControl!
    @IBOutlet weak var ServiceSegment: UISegmentedControl!
    @IBAction func AddCarpets(_ sender: AnyObject) {
    }
    @IBOutlet weak var CarpetsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CarpetsTableView.delegate = self
        self.CarpetsTableView.dataSource = self

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.Close(_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func Close(_ sender: UITapGestureRecognizer) {
     self.dismiss(animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
   // self.BottomLayout.constant = 0
    //self.HeightLayout.constant = 0
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PopUp:UITableViewDelegate , UITableViewDataSource {
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarpetCell", for: indexPath) as! CarpetCell
        
        cell.AttributeLabel.text = " 360 X 120"
        cell.NumberLabel.text = "\(indexPath.row + 1)"

        return cell
 
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }



}

