//
//  ProfileController.swift
//  Wash
//
//  Created by ramy nasser on 9/30/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import TextFieldEffects
import DropDown
import Alamofire
import SwiftyJSON

class ProfileController: UIViewController , UITextFieldDelegate{

    @IBAction func Edit(_ sender: AnyObject) {
        
        UiHelpers.showLoader()

        print("\(self.UserNameField.text!)")
        print("\(self.PasswordField.text!)")
         print("\(self.nameField.text!)")
         print("\(self.EmailField.text!)")				// optional
         print("\(self.PhoneField.text!)")			// optional
         print("\(self.addressField.text!)")			// optional
         print("\(self.SelectGenderButton.titleLabel?.text!)")

        print(userDefaults.string(forKey: DefaultsKeys.useToken)!)
        if let header:HTTPHeaders = ["x-access-Token":userDefaults.string(forKey: DefaultsKeys.useToken)!]
            , let username = self.UserNameField.text
            , let password = self.PasswordField.text
            , let name = self.nameField.text
            , let phone = self.PhoneField.text
            , let email = self.EmailField.text
            , let address = self.addressField.text
            , let gender = self.SelectGenderButton.titleLabel?.text
        
        
        {
        let parmeters = [
            "username": "\(username)",
            "password": "\(password)",
            "name": "\(name)",
            "email": "\(email)",				// optional
            "phone": "\(phone)",				// optional
            "address": "\(address)",				// optional
            "gender": "\(gender)"
        ]
        Alamofire.request(BASEDOMAIN.appending("/profile"), method: .patch, parameters: parmeters, encoding: JSONEncoding.default, headers:header)
            .responseJSON { (response) in
                if let value: AnyObject = response.result.value as AnyObject? {
                    if  response.response?.statusCode == 204 {
                        UiHelpers.hideLoader()
                        DataUtlis.data.SuccessDialog(Title: "نجاح", Body: "تم تغير البيانات بنجاح")
                        let view = self.storyboard!.instantiateViewController(withIdentifier: "CustomSideMenuController")as! CustomSideMenuController
                        self.present(view, animated: true, completion: nil)

                    }else {
                        UiHelpers.hideLoader()

                    print("status code \(response.response?.statusCode)")
                    print("status code \(response.response.debugDescription)")

                    }
                    
                    
                }
        }
        }
     
}



    
    @IBAction func SelectGender(_ sender: AnyObject) {
        self.chooseArticleDropDown.show()
        
    }
    @IBOutlet weak var SelectGenderButton: UIButton!

    @IBOutlet weak var PasswordField: HoshiTextField!
    @IBOutlet weak var PhoneField: HoshiTextField!
    @IBOutlet weak var addressField: HoshiTextField!
    @IBOutlet weak var EmailField: HoshiTextField!
    @IBOutlet weak var nameField: HoshiTextField!
    @IBOutlet weak var UserNameField: HoshiTextField!
    var chooseArticleDropDown = DropDown()
    lazy var cleanerRepository :CleanerRepository = {
        let Repo  = CleanerRepository()
        return Repo
    }()
    var response:ProfileResponseApi?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PasswordField.delegate = self
        self.PhoneField.delegate = self
        self.UserNameField.delegate = self
        self.nameField.delegate = self
        self.addressField.delegate = self
        self.EmailField.delegate = self
        
        
        

        FetchProfile()
        setupChooseArticleDropDown()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    



func textFieldDidEndEditing(_ textField: UITextField) {
    
    self.PasswordField.resignFirstResponder()
    self.PhoneField.resignFirstResponder()
    self.UserNameField.resignFirstResponder()
    self.nameField.resignFirstResponder()
    self.addressField.resignFirstResponder()
    self.EmailField.resignFirstResponder()

    
}

func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    
    self.PasswordField.resignFirstResponder()
    self.PhoneField.resignFirstResponder()
    self.UserNameField.resignFirstResponder()
    self.nameField.resignFirstResponder()
    self.addressField.resignFirstResponder()
    self.EmailField.resignFirstResponder()
    
    return true
    
}



func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        
        if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= keyboardSize.height
        }
    }
}

func keyboardWillHide(notification: NSNotification) {
    
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += keyboardSize.height
            
            
            
        }}
    
}
func dismissKeyboard() {
    //Causes the view (or one of its embedded text fields) to resign the first responder status and drop into background
    view.endEditing(true)
}

override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func setupChooseArticleDropDown() {
        chooseArticleDropDown.anchorView = SelectGenderButton
        
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: SelectGenderButton.bounds.height)
        
        chooseArticleDropDown.dataSource = [
            "Male",
            "Female",
        ]
        
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.SelectGenderButton.setTitle(item, for: .normal)
        }
        
        
    }
    
    
    

}

extension ProfileController {
    func FetchProfile()  {
        UiHelpers.showLoader()
        cleanerRepository.GetProfile { (response, statusCode) in
            switch statusCode {
            case 200 :
                UiHelpers.hideLoader()
                self.response = response
                
                if let name = self.response?.name {
                self.nameField.text = name
                }
                if let address = self.response?.address {
                self.addressField.text = address
                    
                }
                if let phone = self.response?.phone{
                    self.PhoneField.text = phone
                    
                }
                if let email = self.response?.email{
                    self.EmailField.text = email

                }
                if let gender = self.response?.gender{
                    self.SelectGenderButton.setTitle(gender, for: .normal)

                }
                

            default :
                UiHelpers.hideLoader()
                
                DataUtlis.data.ErrorDialog(Title: "خطا في الانترنت", Body: "تاكد من اتصالك بالانترنت")
                
                
            }
            
        }
    }
    
}

