//
//  AddCarpetsVc.swift
//  Wash
//
//  Created by ramy nasser on 9/29/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddCarpetsVc: UIViewController  , ErrorPopupVcDelegate{
    internal func errorPopupVc(width: Int, Height: Int) {
        

        self.Carpets.append(carpet(Width: width, Height: Height))
        self.CarpetsTableView.reloadData()
    }

    var Carpets = [carpet]()
    var location:UserLocation?
    @IBOutlet weak var CarpetsTableView: UITableView!
    @IBAction func Next(_ sender: AnyObject) {
        
        if self.Carpets.count > 0 {
        let view = self.storyboard!.instantiateViewController(withIdentifier: "AddServiceVC")as! AddServiceVC
        view.Carpets = self.Carpets
        view.location = self.location
        self.navigationController?.pushViewController(view, animated: true)
        }else {
        
        DataUtlis.data.ErrorDialog(Title: "خطأ", Body: "اضف سجاد اولا")
        }

    }
    @IBAction func Add(_ sender: AnyObject) {
        var errorPopup:ErrorPopupVc?
        errorPopup = UiHelpers.errorPopupVC
        errorPopup!.delegate = self
        self.addChildViewController(errorPopup!)
        errorPopup!.view.frame = self.view.bounds
        errorPopup!.didMove(toParentViewController: self)
        self.view.addSubview(errorPopup!.view)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CarpetsTableView.delegate = self
        self.CarpetsTableView.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddCarpetsVc:UITableViewDelegate , UITableViewDataSource {
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarpetCell", for: indexPath) as! CarpetCell
        let car = self.Carpets[indexPath.row]
        cell.AttributeLabel.text = "\(car.Width!) X \(car.Height!)"
        cell.NumberLabel.text = "# \(indexPath.row + 1)"
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Carpets.count
    }
    
    
    
}

struct carpet {
    var Width:Int?
    var Height:Int?
    
}

