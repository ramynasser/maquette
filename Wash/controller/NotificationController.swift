//
//  NotificationController.swift
//  GoldenTime
//
//  Created by ramy nasser on 6/20/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
class NotificationController: UITableViewController {
    lazy var cleanerRepository :CleanerRepository = {
        let Repo  = CleanerRepository()
        return Repo
    }()
    var notifs = [NotificationResponseApi]()
    override func viewDidLoad() {
        super.viewDidLoad()
     fetchnotification()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    func fetchnotification() {
        UiHelpers.showLoader()

        cleanerRepository.GetNotifications { (response, StatusCode) in
            switch StatusCode {
            case 200 :
                self.notifs = response!
                self.tableView.reloadData()
                UiHelpers.hideLoader()

                
            default:
                UiHelpers.hideLoader()

                DataUtlis.data.WarningDialog(Title: "فشل بالشبكة", Body: "تاكد من اتصالك بالانترنت")
            
            }
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notifs.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        let not = self.notifs[indexPath.row]
        if indexPath.row == notifs.count-1{
            cell.FirstVerticalView.isHidden = false
            cell.SecondVerticalView.isHidden = true
            cell.imageview.image = UIImage(named: "dot-and-circle")
            
        }else{
            cell.imageview.image = UIImage(named: "circle")
        }
        cell.TitleLabel.text = not.type
        cell.descriptionLabel.text = not.message

        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
