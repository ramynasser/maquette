//
//  AddPaymentVC.swift
//  Wash
//
//  Created by ramy nasser on 9/29/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import DropDown
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON
class AddPaymentVC: UIViewController {
    var chooseArticleDropDown = DropDown()
    var Carpets = [carpet]()
    var location:UserLocation?

    var ServiceType :String?
    var PaymentType :String = "cash"

    var id:String?
    var response = [PriceResponseApi]()
    lazy var cleanerRepository :CleanerRepository = {
        let Repo  = CleanerRepository()
        return Repo
    }()
    @IBOutlet weak var PriceButton: UIButton!
    
    @IBAction func addRequest(_ sender: AnyObject) {
        if let id = self.id  , let lat = self.location?.latitude , let long = self.location?.longitude{
            UiHelpers.showLoader()
            let header:HTTPHeaders = ["x-access-Token":userDefaults.string(forKey: DefaultsKeys.useToken)!]
            
            if let typeOfService = self.ServiceType   {
                let PaymentType = self.PaymentType
                var carpertsDict = [[String:Any]]()
                
                for i in 0...self.Carpets.count-1{
                    let carpet = [
                        "size": self.Carpets[i].Height! * self.Carpets[i].Width!,
                        "quantity" :self.Carpets.count	// optional
                        
                    ]
                    
                carpertsDict.append(carpet)
                }

                let carpet = [
                    "size": self.Carpets[0].Height! * self.Carpets[0].Width!,
                    "quantity" :self.Carpets.count	// optional
                    
                ]
                
              
                let location = [
                    "lang": "\(long)",
                    "lat": "\(lat)"
                ]
                
                let parmeters = [
                    "location":location,
                    "carpets": carpertsDict,
                    "typeOfService": "\(id)",
                    "payment": "\(PaymentType)"
                    ] as [String : Any]

                
                var x = dictToJSON(dict: parmeters as [String : AnyObject])
                print("json from dict \(x)")
                Alamofire.request(BASEDOMAIN.appending("/orders"), method: .post, parameters: parmeters, encoding: JSONEncoding.default, headers:header)
                    .responseJSON { (response) in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            if  response.response?.statusCode == 201 {
                                UiHelpers.hideLoader()
                                DataUtlis.data.SuccessDialog(Title: "نجاح", Body: "تم طلب الخدمة بنجاح")
                                let view = self.storyboard!.instantiateViewController(withIdentifier: "OrdersController")as! OrdersController
                                self.navigationController?.pushViewController(view, animated: true)
                                
                            }else {
                                print("statusCode \(response.response?.statusCode)")
                                print("error in  \(response.response)")
                                print("error in  \(response.response.debugDescription)")

                                UiHelpers.hideLoader()
                                DataUtlis.data.ErrorDialog(Title: "خطا", Body: "شي ما حدث بالخطا")
                                
                            }
                            let post = JSON(value)
                            print(JSON(value))
                            
                            
                            
                        }
                }
            }
        }
        
        
        

        else {
        
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "يجب اختيار السعر اولا ")
        }
        
        
    }
    
    func dictToJSON(dict:[String: Any]) -> Any {
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        let decoded = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        print("decoded Dict \(decoded)")

        return decoded as Any
    }
    
    func arrayToJSON(array:[String]) -> AnyObject {
        let jsonData = try! JSONSerialization.data(withJSONObject: array, options: .prettyPrinted)
        let decoded = try! JSONSerialization.jsonObject(with: jsonData, options: [])
        print("decoded Dict \(array)")

        return decoded as AnyObject
    }
    
    @IBAction func SelectPrice(_ sender: AnyObject) {
        FetchPrice()
        
    }
    @IBAction func selectPayment(_ sender: AnyObject) {
        if PaymentSegment.selectedSegmentIndex == 0 {
            self.PaymentType = "cash"
        }else {
            self.PaymentType = "visa"

        }
        
    }
    @IBOutlet weak var PaymentSegment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupChooseArticleDropDown(data:[PriceResponseApi]) {
        var dataString = [String]()
        
        for i in 0...data.count-1 {
        dataString.append("price \(data[i].priceOfMeter!) $ , time \(data[i].estimatedTime!)")
        }
        chooseArticleDropDown.anchorView = PriceButton
        chooseArticleDropDown.backgroundColor = UIColor(rgb:0xF05D29)
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: PriceButton.bounds.height)
        
        chooseArticleDropDown.dataSource = dataString
        chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
            self.PriceButton.setTitle(item, for: .normal)
            self.id = data[index].id
        }
        
        chooseArticleDropDown.show()
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPaymentVC {
    func FetchPrice()  {
        UiHelpers.showLoader()
        cleanerRepository.GetPrices(callback: { (response, statusCode) in
        
            switch statusCode {
            case 200 :
                UiHelpers.hideLoader()
                self.response = response!
        
                self.setupChooseArticleDropDown(data: self.response)
            default :
                UiHelpers.hideLoader()
                
                DataUtlis.data.ErrorDialog(Title: "خطا في الانترنت", Body: "تاكد من اتصالك بالانترنت")
                
                
            }
            
        })
    
    
    
}
}


