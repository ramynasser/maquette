//
//  SignUpController.swift
//  Wash
//
//  Created by ramy nasser on 9/27/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SwiftyJSON

class SignUpController: UIViewController , UITextFieldDelegate {

    @IBAction func SignUp(_ sender: AnyObject) {
        if validtor() {
            UiHelpers.showLoader()
            
            if let username = self.usernameField.text , let password = self.PasswordField.text , let name = self.nameField.text , let email = EmailField.text{
                let parmeters = [
                    "username": "\(username)",
                    "password": "\(password)",
                    "name": "\(name)",
                    "email":"\(email)"
                ]
                Alamofire.request(BASEDOMAIN.appending("/signup"), method: .post, parameters: parmeters, encoding: JSONEncoding.default)
                    .responseJSON { (response) in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            if  response.response?.statusCode == 400 {
                                UiHelpers.hideLoader()
                            }
                            let post = JSON(value)
                            print(JSON(value))
                            if let token = post["name"].string   {
                                UiHelpers.hideLoader()
                                self.Login()
                            }
                                
                            else {
                                UiHelpers.hideLoader()
                                DataUtlis.data.ErrorDialog(Title: "خطا", Body: "كلمة السر او اسم المستخدم خاطئة")
                                print("error \(response.error)")
                            }
                            
                            
                        }
                }
            }
        }
            
            
            
            
            
        else {
            DataUtlis.data.ErrorDialog(Title: "ERROR", Body: "NOT REGISTERED USER")
            
        }
        

        
    }
    @IBOutlet weak var nameField: HoshiTextField!
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var PhoneField: UITextField!
    @IBOutlet weak var EmailField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.EmailField.delegate = self
        self.PasswordField.delegate = self
        self.PhoneField.delegate = self
        self.usernameField.delegate = self
        self.nameField.delegate = self
        self.EmailField.textColor = UIColor.white
        self.PasswordField.textColor = UIColor.white
        self.PhoneField.textColor = UIColor.white
        self.usernameField.textColor = UIColor.white
        self.nameField.textColor = UIColor.white

        NotificationCenter.default.addObserver(self, selector: #selector(SignUpController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)


        
    }

    
    
    func Login() {
        if let username = self.usernameField.text , let password = self.PasswordField.text {
            let parmeters = [
                "username": "\(username)",
                "password": "\(password)"
            ]
            Alamofire.request(BASEDOMAIN.appending("/login"), method: .post, parameters: parmeters, encoding: JSONEncoding.default)
                .responseJSON { (response) in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        if  response.response?.statusCode == 400 {
                            UiHelpers.hideLoader()
                        }
                        let post = JSON(value)
                        print(JSON(value))
                        if let token = post["accessToken"].string {
                            print("the token y ramy \(token)")
                            UiHelpers.hideLoader()
                            userDefaults.set(token, forKey: DefaultsKeys.useToken)
                            let view = self.storyboard!.instantiateViewController(withIdentifier: "CustomSideMenuController")as! CustomSideMenuController
                            self.present(view, animated: true, completion: nil)
                            
                        }
                            
                        else {
                            UiHelpers.hideLoader()
                            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "كلمة السر او اسم المستخدم خاطئة")
                            print("error \(response.error)")
                        }
                        
                        
                    }
            }
        }
    }
    
    
 
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.usernameField.resignFirstResponder()
        self.EmailField.resignFirstResponder()
        self.PasswordField.resignFirstResponder()
        self.PhoneField.resignFirstResponder()
        self.nameField.resignFirstResponder()

        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        self.usernameField.resignFirstResponder()
        self.EmailField.resignFirstResponder()
        self.PasswordField.resignFirstResponder()
        self.PhoneField.resignFirstResponder()
        self.nameField.resignFirstResponder()

        return true
        
    }
    
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
                
                
                
            }}
        
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status and drop into background
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func validtor() -> Bool {
        var valid = false
        
        if (usernameField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال اسم المستخدم")
            valid = false
            
        }
        if (nameField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال اسم المستخدم")
            valid = false
            
        }
        else if (EmailField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال البريد الالكتروني")
            valid = false
            
        }
        else if (PhoneField.text?.isEmpty)! {
        
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال كلمة المرور")
            valid = false
            
        }
            
            
        else if (PasswordField.text?.isEmpty)! {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "برجاء ادخال تاكيد  كلمة المرور")
            valid = false
            
        }
        else if (PasswordField.text?.characters.count)! < 8 {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "كلمة المرور يجب ان تكون ٨ حروف علي الاقل")
            valid = false
            
        }
        else if PasswordField.text != PhoneField.text {
            
            DataUtlis.data.ErrorDialog(Title: "خطا", Body: "كلمة المرور وتاكيد كلمة المرور غير متشابهين")
            valid = false
            
        }
            
        else {
            valid = true
        }
        return valid
    }


}
