//
//  OrderController.swift
//  Wash
//
//  Created by ramy nasser on 9/29/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit

class OrderController: UIViewController {

    @IBAction func SelectPayment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            NSLog("cash")
        //show popular view
        case 1:
            NSLog("Visa")
        //show history view
        default:
            break
        }
        
    }
    @IBAction func SelectService(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            NSLog("normal")
        //show popular view
        case 1:
            NSLog("Fast")
        //show history view
        default:
            break
        }

    }
    @IBAction func AddCarpet(_ sender: AnyObject) {
    }
    @IBAction func AddOrder(_ sender: AnyObject) {
        
    }
    @IBOutlet weak var CarpetTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
     self.CarpetTableview.delegate = self
     self.CarpetTableview.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.HeightLayout.constant = 0
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OrderController:UITableViewDelegate , UITableViewDataSource {
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarpetCell", for: indexPath) as! CarpetCell
        
        cell.AttributeLabel.text = " 360 X 120"
        cell.NumberLabel.text = "# \(indexPath.row + 1)"
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    
}
