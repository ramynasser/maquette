//
//  MenuController.swift
//  Example
//
//  Created by Teodor Patras on 16/06/16.
//  Copyright © 2016 teodorpatras. All rights reserved.
//

import UIKit
import MessageUI
class MenuController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    let segues = ["showCenterController1" , "showCenterController2" ,"showCenterController4" ,"showCenterController5" , "showCenterController1" ]
    private var previousIndex: NSIndexPath?
    let titles = [
        "طلب خدمة"
        ,"طلباتي"
        ,"الصفحة الشخصية",
        "تواصل معانا",
        "تسجيل الخروج",
       
        
    ]
    
    
    @IBOutlet weak var tableview: UITableView!
    
    let imagesTitles = ["Map","planning","man-user","contact","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        let firstIndexPath = IndexPath(row: 0, section: 0)
        self.tableview.selectRow(at: firstIndexPath, animated: true, scrollPosition: .top)

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 5
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondCell", for: indexPath) as! secondCell
            cell.nameLabel.text = "\(self.titles[indexPath.row])"
            cell.imageview.image = UIImage(named: self.imagesTitles[indexPath.row])
            return cell
            

         }
  

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cellToDeSelect:secondCell = tableView.cellForRow(at: indexPath as IndexPath)! as! secondCell
        cellToDeSelect.contentView.backgroundColor = UIColor.clear
    }

    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath)  {
        var selectedCell:secondCell = tableView.cellForRow(at: indexPath as IndexPath)! as! secondCell
        selectedCell.contentView.backgroundColor = ColorPalette.whiteSmoke
        if indexPath.row <= 3 {
        if let index = previousIndex {
            tableView.deselectRow(at: index as IndexPath, animated: true)
        }

        sideMenuController?.performSegue(withIdentifier: segues[indexPath.row], sender: nil)
        previousIndex = indexPath as NSIndexPath?
        }
        else {
        //Go To login
            let view = self.storyboard!.instantiateViewController(withIdentifier: "LoginController")as! LoginController
            self.present(view, animated: true, completion: nil)
            
            
        }
        
    }

    
    
    
}


