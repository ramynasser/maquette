//
//  ViewController.swift
//  Wash
//
//  Created by ramy nasser on 9/27/17.
//  Copyright © 2017 RamyNasser. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func GoToSignUp(_ sender: AnyObject) {
        let view = self.storyboard!.instantiateViewController(withIdentifier: "SignUpController")as! SignUpController
        self.navigationController?.pushViewController(view, animated: true)

    }
    @IBAction func GotoLogin(_ sender: AnyObject) {
        let view = self.storyboard!.instantiateViewController(withIdentifier: "LoginController")as! LoginController
        self.navigationController?.pushViewController(view, animated: true)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

