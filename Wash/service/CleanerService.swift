//
//  CarService.swift
//  Kia
//
//  Copyright © 2017 Code95. All rights reserved.
//

import Foundation
import Moya
import Gloss
enum CleanerService {
    
    case getProfile()
    case gerOrder()
    case getPrice()
    case getNotification()

    //`/videos/:videoId/comments`
    
    //Comment
}

extension CleanerService : TargetType{
    
    
    
    var baseURL: URL { return URL(string: BASEDOMAIN)! }
    
    var path:String{
        switch self {
        case .getProfile:
            return "/profile"
        case .gerOrder:
            return "/orders"
        case .getPrice:
            return "/prices"
        case .getNotification:
           return  "/notifications"
        }
        
    }
    
    var method: Moya.Method {
        switch self {
        case .getProfile,.gerOrder , .getPrice,.getNotification:
            return .get
            
            
        }
    }
    var parameters: [String: Any]? {
        
        switch self {
            
        case .getProfile,.gerOrder , .getPrice,.getNotification:
            return nil

            
        }
        
        
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getProfile,.gerOrder,.getPrice,.getNotification:
            return JSONEncoding.default
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    
    var task: Task {
        return .request
    }
    
    
    
}



