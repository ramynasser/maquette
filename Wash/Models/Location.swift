//
//	Location.swift
//
//	Create by okk on 30/9/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - Location
public struct Location: Glossy {

	public let lang : String!
	public let lat : String!



	//MARK: Decodable
	public init?(json: JSON){
		lang = "lang" <~~ json
		lat = "lat" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"lang" ~~> lang,
		"lat" ~~> lat,
		])
	}

}