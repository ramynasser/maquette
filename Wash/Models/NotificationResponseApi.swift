//
//	RootClass.swift
//
//	Create by okk on 7/10/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation
import Gloss

//MARK: - RootClass
public struct NotificationResponseApi: Glossy {
    
    public let id : String!
    public let message : String!
    public let read : Bool!
    public let relatedId : String!
    public let type : String!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        id = "id" <~~ json
        message = "message" <~~ json
        read = "read" <~~ json
        relatedId = "relatedId" <~~ json
        type = "type" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "message" ~~> message,
            "read" ~~> read,
            "relatedId" ~~> relatedId,
            "type" ~~> type,
            ])
    }
    
}
