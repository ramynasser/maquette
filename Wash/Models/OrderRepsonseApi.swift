//
//	RootClass.swift
//
//	Create by okk on 30/9/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - RootClass
public struct OrderRepsonseApi: Glossy {

	public let activeStatus : String!
	public let carpets : [Carpet]!
	public let createdAt : String!
	public let customer : String!
	public let deliverer : AnyObject!
	public let destination : AnyObject!
	public let id : String!
	public let location : Location!
	public let payment : String!
	public let price : Int!
	public let status : [Statu]!
	public let typeOfService : TypeOfService!
	public let updatedAt : String!



	//MARK: Decodable
	public init?(json: JSON){
		activeStatus = "activeStatus" <~~ json
		carpets = "carpets" <~~ json
		createdAt = "createdAt" <~~ json
		customer = "customer" <~~ json
		deliverer = "deliverer" <~~ json
		destination = "destination" <~~ json
		id = "id" <~~ json
		location = "location" <~~ json
		payment = "payment" <~~ json
		price = "price" <~~ json
		status = "status" <~~ json
		typeOfService = "typeOfService" <~~ json
		updatedAt = "updatedAt" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"activeStatus" ~~> activeStatus,
		"carpets" ~~> carpets,
		"createdAt" ~~> createdAt,
		"customer" ~~> customer,
		"deliverer" ~~> deliverer,
		"destination" ~~> destination,
		"id" ~~> id,
		"location" ~~> location,
		"payment" ~~> payment,
		"price" ~~> price,
		"status" ~~> status,
		"typeOfService" ~~> typeOfService,
		"updatedAt" ~~> updatedAt,
		])
	}

}
