//
//	RootClass.swift
//
//	Create by okk on 4/10/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation
import Gloss

//MARK: - RootClass
public struct PriceResponseApi: Glossy {
    
    public let createdAt : String!
    public let estimatedTime : String!
    public let id : String!
    public let priceOfMeter : Int!
    public let typeOfService : String!
    public let updatedAt : String!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        createdAt = "createdAt" <~~ json
        estimatedTime = "estimatedTime" <~~ json
        id = "id" <~~ json
        priceOfMeter = "priceOfMeter" <~~ json
        typeOfService = "typeOfService" <~~ json
        updatedAt = "updatedAt" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "estimatedTime" ~~> estimatedTime,
            "id" ~~> id,
            "priceOfMeter" ~~> priceOfMeter,
            "typeOfService" ~~> typeOfService,
            "updatedAt" ~~> updatedAt,
            ])
    }
    
}
