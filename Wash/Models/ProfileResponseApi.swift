//
//	RootClass.swift
//
//	Create by okk on 30/9/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation
import Gloss

//MARK: - RootClass
public struct ProfileResponseApi: Glossy {
    
    public let address : String!
    public let createdAt : String!
    public let email : String!
    public let gender : String!
    public let id : String!
    public let name : String!
    public let phone : String!
    public let updatedAt : String!
    public let user : String!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        address = "address" <~~ json
        createdAt = "createdAt" <~~ json
        email = "email" <~~ json
        gender = "gender" <~~ json
        id = "id" <~~ json
        name = "name" <~~ json
        phone = "phone" <~~ json
        updatedAt = "updatedAt" <~~ json
        user = "user" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "address" ~~> address,
            "createdAt" ~~> createdAt,
            "email" ~~> email,
            "gender" ~~> gender,
            "id" ~~> id,
            "name" ~~> name,
            "phone" ~~> phone,
            "updatedAt" ~~> updatedAt,
            "user" ~~> user,
            ])
    }
    
}
