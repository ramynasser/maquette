//
//	Carpet.swift
//
//	Create by okk on 30/9/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - Carpet
public struct Carpet: Glossy {

	public let id : String!
	public let quantity : Int!
	public let size : Int!



	//MARK: Decodable
	public init?(json: JSON){
		id = "_id" <~~ json
		quantity = "quantity" <~~ json
		size = "size" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"_id" ~~> id,
		"quantity" ~~> quantity,
		"size" ~~> size,
		])
	}

}