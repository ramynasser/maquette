//
//  UserAccountRepo.swift
//  Kia
//
//  Created by Magdy Zamel on 6/27/17.
//  Copyright © 2017 Code95. All rights reserved.
//

import Foundation
import Moya
import Moya_Gloss

class CleanerRepository {
    //AddCommentResponseApi
    
    private var provider :MoyaProvider<CleanerService>!
    private var Token = userDefaults.string(forKey: DefaultsKeys.useToken)
    
    
    init() {
        print(Token)
        let endpointClosure = { (target: CleanerService) -> Endpoint<CleanerService> in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(parameters: nil, httpHeaderFields: ["x-access-Token":self.Token!,"Content-Type": "application/json"], parameterEncoding:  URLEncoding.queryString)
        }
        provider = MoyaProvider<CleanerService>(endpointClosure: endpointClosure)
        
    }
    //groupBy
    private func configProviderWithToken() {
        let endpointClosure = { (target: CleanerService) -> Endpoint<CleanerService> in
            
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            return defaultEndpoint.adding(parameters: nil, httpHeaderFields:
                [
                    "x-access-Token":
                        self.Token!], parameterEncoding:  URLEncoding.queryString)
        }
        provider = MoyaProvider<CleanerService>(endpointClosure: endpointClosure)
        
    }
    
   
    
    
    
       func GetProfile(callback: @escaping (_ Repsonse:ProfileResponseApi?,_ status:Int)-> Void) {
        self.provider.request(.getProfile()) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("done")
                    debugPrint(try?response.mapJSON())
                    let Repsonse = try response.mapObject(ProfileResponseApi.self)
                    callback(Repsonse,response.statusCode)
                } catch {
                    callback(nil,response.statusCode)
                }
            case .failure:
                callback(nil,404)
            }
        }
        
    }
    
    
    
    
    func GetOrders(callback: @escaping (_ Repsonse:[OrderRepsonseApi]?,_ status:Int)-> Void) {
        self.provider.request(.gerOrder()) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("done")
                    debugPrint(try?response.mapJSON())
                    let Repsonse = try response.mapArray(OrderRepsonseApi.self)
                    callback(Repsonse,response.statusCode)
                } catch {
                    callback(nil,response.statusCode)
                }
            case .failure:
                callback(nil,404)
            }
        }
        
    }

    
    func GetPrices(callback: @escaping (_ Repsonse:[PriceResponseApi
        ]?,_ status:Int)-> Void) {
        configProviderWithToken()
        self.provider.request(.getPrice()) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("done")
                    debugPrint(try?response.mapJSON())
                    let Repsonse = try response.mapArray(PriceResponseApi.self)
                    callback(Repsonse,response.statusCode)
                } catch {
                    callback(nil,response.statusCode)
                }
            case .failure:
                callback(nil,404)
            }
        }
        
    }
    
    
    
    func GetNotifications(callback: @escaping (_ Repsonse:[NotificationResponseApi
        ]?,_ status:Int)-> Void) {
        configProviderWithToken()
        self.provider.request(.getNotification()) { (result) in
            switch result {
            case .success(let response):
                do {
                    print("done")
                    debugPrint(try?response.mapJSON())
                    let Repsonse = try response.mapArray(NotificationResponseApi.self)
                    callback(Repsonse,response.statusCode)
                } catch {
                    callback(nil,response.statusCode)
                }
            case .failure:
                callback(nil,404)
            }
        }
        
    }

}
